/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx}"],
  mode: "jit",
  theme: {
    extend: {
      colors: {
        primary: "#00040f",
        secondary: "#00f6ff",
        dimWhite: "rgba(255, 255, 255, 0.7)",
        dimBlue: "rgba(9, 151, 124, 0.1)",
        
      },
      fontFamily: {
        rubik: ["Rubik","sans-serif"],
      },
      backgroundImage: {
        'mob': "url('assets/bgmob.png')",
        'des': "url('assets/bglg.png')",
        'but': "url('assets/button.png')",
        'gray': "url('assets/graybg.png')",
        'graymb': "url('assets/graymb.png')",
        'ctades': "url('assets/ctades.png')",
        'ctamob': "url('assets/ctamob.png')",
        'footer': "url('assets/footer.png')",
        'footerlogo': "url('assets/footerlogo.png')",
        'footer1': "url('assets/footer1.jpg')"
      
      }
    },
    screens: {
      'sm': {'min': '1px', 'max': '767px'},
      'md': {'min': '768px', 'max': '1023px'},
      'lg': {'min': '1024px', 'max': '1279px'},
      'xl': {'min': '1280px', 'max': '1535px'},
      '2xl': {'min': '1536px'},
    },
  },
  plugins: [],
};

