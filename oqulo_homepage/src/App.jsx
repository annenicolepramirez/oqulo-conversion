import styles from './style';
import { Navbar, Banner, Intro, Stats, CTA, Footer } from "./components";
import Features from './components/Features';

const App = () => {
  return (
    <section>
      <div className='bg-des sm:bg-mob bg-cover bg-norepeat w-full'>
        {/*div for navbar*/}
        <div className='2xl:px-96 2xl:py-3 xl:px-52 xl:py-10 lg:px-36 lg:py-5 md:px-28 md:py-2 sm:px-5'>
          <Navbar />
        </div>
        {/*div for banner*/}
        <div className='2xl:ml-96 2xl:mr-96 xl:ml-72 xl:mr-52 lg:ml-48 lg:mr-36 md:mr-24 md:ml-36'>
          <Banner />
        </div>
      </div>

      
      {/*div for intro*/}
      <div className='mt-24 mb-24 2xl:ml-96 2xl:mr-96 xl:ml-72 xl:mr-52 lg:ml-48 lg:mr-36 md:mr-24 md:ml-28 md:mt-14 sm:mx-6'>
        <Intro />
      </div>
      {/*div for features*/}
      <div className='bg-gray sm:bg-graymb'>
        <div className='pt-20 mt-20 2xl:ml-96 2xl:mr-96 xl:ml-72 xl:mr-52 lg:ml-48 lg:mr-36 md:mr-24 md:ml-28 md:mt-10 md:pt-6'>
          <Features />
        </div>
      </div>
      {/*div for Stats*/}
      <div>
        <div className='mt-44 mb-24 2xl:ml-96 2xl:mr-96 xl:ml-72 xl:mr-52 lg:ml-48 lg:mr-36 md:mr-24 md:ml-28 md:mb-14 md:mt-10 md:pt-6 sm:mt-30'>
          <Stats/>
        </div>
      </div>
      {/*div for CTA*/}
      <div className='bg-ctades sm:bg-ctamob bg-cover bg-norepeat w-full'>
        <div className='pt-40 pb-40 2xl:ml-96 2xl:mr-96 xl:ml-72 xl:mr-52 lg:ml-48 lg:mr-36 md:mr-24 md:ml-28 md:pb-16 md:mt-6 md:pt-2'>
          <CTA />
        </div>
      </div>
      {/*div for footer*/}
      <div className='2xl:bg-footer xl:bg-footer lg:bg-footer md:bg-footer bg-[length:100%_100%] bg-no-repeat sm:bg-white  w-full'>
        <div className='pt-40 pb-48 2xl:ml-96 2xl:mr-96 xl:ml-72 xl:mr-52 lg:ml-48 lg:mr-36 md:mr-24 md:ml-28 md:mt-10 md:pt-6'>
          <Footer />
        </div>
      </div>
    </section>
    
  )
}

export default App
