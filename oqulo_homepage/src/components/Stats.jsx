import React from 'react'
import { statsdes,statsmob } from '../assets'

const Stats = () => {
    return (
        <div className='font-rubik leading-loose'>
            <div className='text-center sm:p-4'>
                <h1 className='font-bold text-4xl tracking-wide mb-2 md:text-2xl md:mb-2 md:mt-4 sm:text-3xl'>Stats Delivered Beautifully</h1>
                <h2 className='md:text-[12px]'>View sales charts, booking ratio and <br className='hidden sm:block'/> user behavior using Oqulo's visual <br className='hidden sm:block' /> reporting feature</h2>
            </div>

            <div className='sm:hidden mt-20 md:mt-8 w-[90%] mx-auto'>
                <img src={statsdes}/>
            </div>

            <div className='hidden sm:block'>
                <img src={statsmob} className='mx-auto w-[450px]'/>
            </div>
        </div>
    )
}

export default Stats
