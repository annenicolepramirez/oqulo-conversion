import { useState } from 'react'
import { navLinks } from '../constants';
import { close, logo, menu } from '../assets';

const Navbar = () => {
    const [toggle, setToggle] = useState(false);
    return (
      <nav className='w-full flex py-2 justify-between items-center navbar '>
        <img src={logo} alt="hoobank" className='2xl:w-[125px] lg:w-[120px] md:w-[95px] sm:w-[120px] sm:pt-4' />
        <ul className='list-none flex sm:hidden justify-end items-center flex-1'>
          {navLinks.map((nav, index) => (
            <li key={nav.id}
              className={`font-rubik font-normal cursor-pointer 2xl:text-[16px] ${index === navLinks.length - 1 ? 'mr-0' : 'mr-10 lg:mr-5 md:mr-2'} text-white lg:text-[15px] md:text-[12px]`}>
              <a href={`#${nav.id}`}>
                {nav.title}
              </a>
            </li>
          ))}
        </ul>

        <div className='sm:flex hidden flex flex-1 justify-end items-center'>
          <img
            src={toggle ? close : menu}
            alt="menu"
            className='w-[35px] object-contain'
            onClick={() => setToggle((prev) => !prev)}
          />
  
          <div className={`${toggle ? 'flex' : 'hidden'} p-6 bg-fuchsia-900 absolute top-20 right-0 mx-4 my-2 min-w-[140px] rounded-xl sidebar`}>
            <ul className='list-none flex flex-col justify-end items-center flex-1'>
              {navLinks.map((nav, index) => (
                <li key={nav.id}
                  className={`font-rubik font-normal cursor-pointer text-[16px] ${index === navLinks.length - 1 ? 'mr-0' : 'mb-4'} text-white`}>
                  <a href={`#${nav.id}`}>
                    {nav.title}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>

  
      
      </nav>
    )
  }
  
  export default Navbar
  
