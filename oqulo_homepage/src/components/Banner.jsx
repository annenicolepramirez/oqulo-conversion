import React from 'react'
import { ip7 } from '../assets'
const Banner = () => {
  return (
    <div className='grid 2xl:grid-cols-3 xl:grid-cols-3 lg:grid-cols-3 md:grid-cols-3 sm:auto-rows-max sm:h-[100%]'>
      <div className=' sm:order-2 sm:col-span-2 '>
        <img src={ip7} className='2xl:mx-32 2xl:w-[350px] xl:mx-12 xl:w-[250px] lg:w-[225px] md:w-[215px] sm:mx-auto sm:w-[77%] sm:mt-6 ' />
      </div>
      <div className='col-span-2 sm:order-1'>
        <h1 className='text-white text-justify font-rubik px-5 font-bold 2xl:text-4xl 2xl:pl-40 xl:text-4xl lg:text-3xl md:text-[19.5px] sm:text-2xl sm:mt-6 sm:text-left sm:pl-6'>The Only Platform You’ll Need to Run Smart Coworking Spaces & Serviced Offices</h1>
        <br />
        <div className='text-white font-rubik mb-2 2xl:leading-7 2xl:text-1xl 2xl:pl-40 xl:px-6 lg:px-4 lg:text-[12px] md:px-2 md:text-[9px] md:pl-6 sm:text-[16.5px] sm:text-left sm:px-6'>
          Oqulo is built to sell, manage and grow your commercial real estate business.<br className='sm:hidden'/>
          <br className='hidden sm:block'/>Collect payments, manage clients and run reports using our booking app.<br className='sm:hidden' />
          <br className='hidden sm:block'/>Engage members using our community messaging feature.<br className='sm:hidden' />
        </div>
        <div className='text-white font-rubik mb-2 2xl:mt-6 2xl:text-1xl 2xl:pl-40 xl:px-6 lg:px-4 lg:text-[13px] md:text-[9px] md:pl-6 sm:text-[16.5px] sm:px-6 '> Be the first in line to take Oqulo for a <br className='hidden sm:block'/> test drive! </div>
        <div className='2xl:pl-40 xl:pl-6 lg:pl-4 md:pl-6 sm:px-6'>
          <input type="text" className='rounded-full text-[23px] w-[16em] py-1 xl:w-[18em] xl:text-[12px] lg:text-[11px] md:text-[8px] sm:text-[19px]' placeholder='&emsp;Email address'></input>
          <button class="text-white bg-but bg-cover bg-no-repeat rounded-full px-6 py-2 ml-2 2xl:text-[18px] xl:text-[12px] xl:py-1 lg:text-[11px] lg:py-1 md:text-[10px] md:py-1 sm:m-0 sm:mt-3">NOTIFY ME</button>
        </div>
        <div className='text-white font-rubik opacity-75 2xl:text-1xl 2xl:pl-40 xl:text-[12px] xl:pl-6 lg:text-[10px] lg:pl-4 md:text-[8px] md:pl-6  sm:text-sm sm:px-6 sm:mt-3'>
          *No spam, that’s a promise.
        </div>



      </div>


    </div>
  )
}

export default Banner
