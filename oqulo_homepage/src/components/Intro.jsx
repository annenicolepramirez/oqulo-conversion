import React from 'react'
import { phoneimg } from '../assets'

const Intro = () => {
  return (
    <div className='font-rubik'>
      <div className="flex flex-row sm:hidden">
        <div className='grow-0'>
          <h1 className='font-medium 2xl:text-5xl xl:text-4xl lg:text-3xl md:text-1xl'>Tried & Tested Space Management Software</h1>

          <br />
          <p className='lg:text-[13px] md:text-[8px]'>Oqulo is a homegrown app that’s been tested by <br/>
            real-life businesses. Whether you operate on a single <br/>
            building or in multiple locations, Oqulo is designed to  <br/>
            make your space leasing operations hassle-free.</p>


          <br />
          <p className='lg:text-[13px] md:text-[8px]'>Your clients will have a smooth booking & online <br/>
            payment experience, and your concierge staff will be  <br/>
            able to view occupancy stats and generate reports at a  <br />
            click of a button. </p>

        </div>

        <div className='grow'>
          <img src={phoneimg} className='mx-auto w-[100%] md:w-[65%]' />
        </div>
      </div>

      <div class="hidden sm:flex sm:flex-col">
        <div className='pb-6 font-rubik font-bold text-2xl'>Tried & Tested Space Management Software</div>
        <div><img src={phoneimg} className='mx-auto w-[100%] md:w-[80%] pb-6' /></div>
        <div>
          <p className='lg:text-[13px] md:text-[7px] sm:text-[12px] font-rubik'>Oqulo is a homegrown app that’s been tested by
            real-life businesses. Whether you operate on a single
            building or in multiple locations, Oqulo is designed to 
            make your space leasing operations hassle-free.</p>

          <br/>
          <p className='lg:text-[13px] md:text-[7px] sm:text-[12px] font-rubik'>Your clients will have a smooth booking & online
            payment experience, and your concierge staff will be 
            able to view occupancy stats and generate reports at a  
            click of a button. </p>
        </div>
      </div>

    </div>




  )
}

export default Intro