import React from 'react'
import { graybg, pen, hand, finger, lock, screen } from '../assets'
const Features = () => {
    return (
        <div>
            <div className='font-rubik 2xl:h-[33em] xl:h-[29em] lg:h-[24em] md:h-[19.5em] pt-4 sm:hidden'>
                <div className='text-center mb-8 '>
                    <h1 className='font-rubik text-4xl md:text-2xl lg:text-2xl mb-1'>Oqulo Features at a Glance</h1>
                    <h1 className='font-rubik md:text-[11px] lg:text-[11.5px]'>Powerful functionalities that changes the way you do business.</h1>
                </div>

                <div className='grid grid-cols-8 gap-1'>
                    <div className='col-span-2 text-right mt-6 md:mt-2'>
                        <h1 className='font-rubik font-bold lg:text-[10px] md:text-[9px]'>Powerful Space Management</h1>
                        <p className='font-rubik 2xl:text-[13px] xl:text-[12px] lg:text-[10px] md:text-[7px]'>Manage meeting room and desk bookings, create events, sell tickets, schedule private office showings, automate invoicing and connect with members --- all in one central dashboard.</p>

                    </div>

                    <div>
                        <img src={finger} className='mx-auto w-[30px] mt-8 lg:w-[20px] md:w-[18px] md:mt-4' />
                    </div>

                    <div className='col-span-2 row-span-2'>
                        <img src={screen} className='w-[900px] lg:mt-4' />
                    </div>

                    <div>
                        <img src={pen} className='mx-auto w-[30px] mt-8 md:w-[20px] md:mt-4 lg:w-[20px]' />
                    </div>

                    <div className='col-span-2 mt-6 md:mt-2'>
                        <h1 className='font-rubik font-bold md:text-[9px] lg:text-[10px]' >User-Friendly Interface</h1>
                        <p className='2xl:text-[13px] xl:text-[12px] md:text-[7px] lg:text-[10px]'>Clients will find it easy to book and pay for their space, thanks to Oqulo’s easy navigation and pixel-perfect design. Keep members up to date with Oqulo’s community board and help desk features.</p>
                    </div>

                    <div className='col-span-2 text-right'>
                        <h1 className='font-rubik font-bold md:text-[9px] lg:text-[10px]'>Painless Integration</h1>
                        <p className='2xl:text-[13px] xl:text-[12px] md:text-[7px] lg:text-[10px]'>No matter what your website is built on, Oqulo is easy to setup and integrate with CRM and payment gateways. Go live in a matter of days.</p>
                    </div>

                    <div>
                        <img src={hand} className='mx-auto w-[40px] mt-2 md:w-[25px] md:mt-2 lg:w-[28px]' />
                    </div>

                    <div>
                        <img src={lock} className='mx-auto w-[30px] mt-2 md:w-[19px] md:mt-2 lg:w-[20px]' />
                    </div>

                    <div className='col-span-2'>
                        <h1 className='font-rubik font-bold md:text-[9px] lg:text-[10px]'>Secure Data & White Label <br/> Branding</h1>
                        <p className='2xl:text-[13px] xl:text-[12px] md:text-[7px] lg:text-[10px]'>Get peace of mind in knowing that client information and sales data are stored in a secure server. Our white label service allows you to market this platform as your own.</p>
                    </div>
                </div>
            </div>

            <div className='hidden sm:block px-6 h-[77em]'>
                <div className='text-center'>
                    <h1 className='font-rubik text-2xl font-bold'>Oqulo Feature <br /> at a Glance</h1>
                    <h2 className='font-rubik text-sm'>Powerful functionalities that changes <br /> the way you do business.</h2>
                </div>

                <div className='grid grid-cols-3 p-4  mt-5 gap-4'>

                    <div>
                        <img src={finger} className='w-[30px]' />
                    </div>
                    <div className='col-span-2'>
                        <h1 className='font-rubik text-md font-bold'>Powerful Space Management</h1>
                        <p className='font-rubik text-sm'>Manage meeting room and desk bookings, create events, sell tickets, schedule private office showings, automate invoicing and connect with members --- all in one <br /> central dashboard.</p>
                    </div>

                    <div>
                        <img src={hand} className='w-[40px] ' />
                    </div>
                    <div className='col-span-2'>
                        <h1 className='font-rubik text-md font-bold'>Painless Integration</h1>
                        <p className='font-rubik text-sm'>No matter what your website is built on, Oqulo is easy to setup and integrate with CRM and payment gateways. Go live in a matter of days.</p>
                    </div>

                    <div>
                        <img src={pen} className='w-[30px] ' />
                    </div>
                    <div className='col-span-2'>
                        <h1 className='font-rubik text-md font-bold'>User-Friendly Interface</h1>
                        <p className='font-rubik text-sm'>Clients will find it easy to book and pay for their space, thanks to Oqulo’s easy navigation and pixel-perfect design. Keep members up to date with Oqulo’s community board and help desk features.</p>
                    </div>

                    <div>
                        <img src={lock} className='w-[30px] ' />
                    </div>

                    <div className='col-span-2'>
                        <h1 className='font-rubik text-md font-bold'>Secure Data & White Label Branding</h1>
                        <p className='font-rubik text-sm'>Get peace of mind in knowing that client information and sales data are stored in a secure server. Our white label service allows you to market this platform as your own.</p>
                    </div>
                </div>

                <div className='mt-3'>
                    <img src={screen} className='mx-auto w-[250px]' />
                </div>
                
            </div>
        </div>

    )
}

export default Features
