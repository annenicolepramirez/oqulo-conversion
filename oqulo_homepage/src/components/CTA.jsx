import React from 'react'

const CTA = () => {
    return (
        <div className='text-center font-rubik text-white md:pt-10'>
            <div className='mb-6'>
                <h1 className='font-bold text-4xl mb-2 md:text-3xl '>Launching Soon</h1>
                <p className='text-[20px] md:text-[15px] mb-8 md:mb-2'>Sign up to get updates on Oqulo’s <br className='hidden sm:block'/> public release.</p>
            </div>
            <div>
                <input type="text" className='rounded-full text-[23px] w-[16em] py-1 xl:w-[18em] xl:text-[12px] lg:text-[11px] md:text-[8px] sm:text-[19px]' placeholder='&emsp;Email address'></input>
                <button class="text-white bg-but bg-cover bg-no-repeat rounded-full px-6 py-2 ml-2 2xl:text-[18px] xl:text-[12px] xl:py-1 lg:text-[11px] lg:py-1 md:text-[10px] md:py-1 sm:m-0 sm:mt-3">TRY THE BETA</button>
            </div>
        </div>
    )
}

export default CTA
