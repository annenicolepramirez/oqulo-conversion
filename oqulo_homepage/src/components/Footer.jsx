import React from 'react'
import { navLinks } from '../constants';
import { footerlogo } from '../assets'
const Footer = () => {
    return (
        <div>
            <div>
                <img src={footerlogo} className='mx-auto' />
            </div>
            <br />
            <br />
            <div className='text-center sm:hidden'>
                <ul className='list-none flex justify-center items-center flex-1'>
                    {navLinks.map((nav, index) => (
                        <li key={nav.id}
                            className={`font-bold font-rubik  cursor-pointer 2xl:text-[16px]  ${index === navLinks.length - 1 ? 'mr-0' : 'mr-10 lg:mr-5 md:mr-2'} text-black lg:text-[15px] md:text-[12px]`}>
                            <a href={`#${nav.id}`}>
                                {nav.title}
                            </a>
                        </li>
                    ))}
                </ul>
            </div>

            <div className='hidden sm:block'>
                <ul className='text-center'>
                    {navLinks.map((nav, index) => (
                        <p key={nav.id}
                            className={`font-bold font-rubik leading-8 cursor-pointer 2xl:text-[16px] text-black lg:text-[15px] md:text-[12px]`}>
                            <a href={`#${nav.id}`}>
                                {nav.title}
                            </a>
                        </p>
                    ))}

                </ul>

            </div>


            <br />
            <br />
            <div className='font-rubik text-center text-md'>
                Copyright &copy; Oqulo 2018. All rights reserved.
            </div>
        </div>
    )
}

export default Footer
