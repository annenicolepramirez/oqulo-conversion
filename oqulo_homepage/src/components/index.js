import Navbar from "./Navbar";
import Banner from "./Banner";
import Intro from "./Intro";
import Features from "./Features";
import Stats from "./Stats";
import CTA from "./CTA";
import Footer from "./Footer";
export {
    Navbar,
    Banner,
    Intro,
    Features,
    Stats,
    CTA,
    Footer
};