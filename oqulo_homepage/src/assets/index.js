import logo from './logo.png';
import bglg from './bglg.png';
import bgmob from './bgmob.png';
import ip7 from './ip7.png';
import close from './close.svg';
import menu from './menu.svg';
import but from './button.png';
import phoneimg from './phoneimg.png';
import graybg from './graybg.png';
import pen from './pen.png';
import hand from './hand.png';
import finger from './finger.png';
import lock from './lock.png';
import graymb from './graymb.png';
import screen from './screen.png';
import statsdes from './statsdes.png';
import statsmob from './statsmob.png';
import ctades from './ctades.png';
import ctamob from './ctamob.png';
import footer from './footer.png';
import footerlogo from './footerlogo.png';
import footer1 from './footer1.jpg';

export {
    logo,
    bglg,
    bgmob,
    ip7,
    menu,
    close,
    but,
    phoneimg,
    graybg,
    pen,
    hand,
    finger,
    lock,
    graymb, 
    screen,
    statsdes,
    statsmob,
    ctades,
    ctamob,
    footer,
    footerlogo,
    footer1
}