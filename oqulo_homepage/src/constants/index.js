export const navLinks = [
    {
        id: "discover",
        title: "DISCOVER OQULO",
    },
    {
        id: "features",
        title: "FEATURES",
    },
    {
        id: "contact",
        title: "CONTACT",
    }
];